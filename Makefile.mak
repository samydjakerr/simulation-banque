# Répertoire contenant les fichiers source .cpp
SRC_DIR := src

# Répertoire contenant les fichiers d'en-tête .hpp
INCLUDE_DIR := src/include

# Obtenez la liste des fichiers source .cpp
CPP_FILES := $(wildcard $(SRC_DIR)/*.cpp)

# Générez la liste des fichiers objets .o correspondants aux fichiers source .cpp
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,%.o,$(CPP_FILES))

# Options de compilation
CC := g++
CFLAGS := -std=c++11 -Wall -pedantic -I$(INCLUDE_DIR)

# Nom de l'exécutable
TARGET := main.out

# Règle par défaut pour construire l'exécutable
all: $(TARGET)

# Règle pour construire l'exécutable à partir des fichiers objets
$(TARGET): $(OBJ_FILES)
	$(CC) $^ -o $@

# Règle générique pour générer les fichiers objets .o à partir des fichiers source .cpp
%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

# Nettoyage des fichiers objets et de l'exécutable
clean:
	rm -f $(OBJ_FILES) $(TARGET)

