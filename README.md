<<<<<<< README.md
# Étude de cas : Simulation de Banque

## Introduction
L'objectif de cette étude de cas est de simuler le fonctionnement d'une banque, y compris un nombre fixe de caissiers et l'arrivée de clients. Lorsqu'un client arrive, s'il y a un caissier disponible, il est pris en charge immédiatement. Sinon, le client rejoint une file d'attente commune à tous les caissiers.

## Le but
Le but de la simulation est de collecter des données statistiques sur différents aspects du système bancaire simulé.
https://depinfo.u-cergy.fr/~pl/docs/sujetBanque.pdf

## Scénario
### Arrivée d'un client :
* Un client arrive ;
* Si un caissier est libre, le client se fait servir par le premier caissier libre ; 
* Sinon, le client rejoint la file d'attente.

### Libération d'un caissier :
* Si la file d'attente est vide, le caissier vas attendre ;
* Sinon, le premier client quitte la file et le caissier le sert.


## Hypothèses
* Les clients sont honnêtes et respectent l'ordre d'arrivée dans la file d'attente.;
* Les clients sont patients et attendent leur tour sans quitter la banque avant d'être servis.;
* Les clients sont répartis de manière équitable entre les caissiers disponibles. ;
* Les caissiers ne sont jamais fatigués et servent immédiatement le client suivant en file.
* La simulation repose sur la modélisation d'événements discrets, ce qui signifie que le temps s'écoule de manière discrète entre deux événements consécutifs.
    

### Paramètres d'entrée :
* Durée prévue de la simulation;
* Le nombre de caissiers disponibles ;
* Le temps moyen de service de chaque caissier (en supposant que les caissiers ne sont pas tous également performants);
* Temps moyen entre deux arrivées successives de clients.

### Paramètre de sortie :
* La durée réelle de la simulation, c'est-à-dire le moment où tous les clients restants dans la file ont été servis ;
* Les longueurs maximale et moyenne de la file d'attente ;
* Le nombre total de clients servis, ainsi que le nombre de clients servis par caissier;
* Le taux d'occupation de chaque caissier;
* Le temps moyen d'attente d'un client dans la file.

## Rendu du projet
### Date
* 29/09/2023
### Livrables
Archive contenant :
* Les codes sources du projet ;
* La documentation générée par Doxygen ;
* Un Makefile.


## Makefile
Compilation/build sous Windows:
```
mingw32-make -f Makefile.mak
```

Lancement du programme :
```
.\main.out
```
## Doxygen
Générer la documentation :
```
doxygen DoxyFile
```
Elle est accessible au format `html` au chemin `docs/index.html`.

## Sortie du programme
Le programme affiche :
* Le déroulé de la simulation recensant les évenements d'arrivée et de départ des clients ainsi que leurs positions dans la file d'attente ;
* Les paramètres d'entrées de la simulation ;
* Les statisques globales de la simulation;
* Les statisques caissiers ;
=======
# Simulation-banque

