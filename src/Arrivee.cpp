/// @file Arrivee.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Arrivee, dérivée de la classe Evenement
/// @date 2023

#include "include/Arrivee.hpp"
#include "include/Banque.hpp"


#include <iostream>
using namespace std;


Arrivee::Arrivee(double _heureEvenement, Banque* pBanque):Evenement(_heureEvenement, pBanque) {
    this->_heureEvenement = _heureEvenement; 
    this->pBanque = pBanque;
}
/**
* @brief Traite l'événement d'arrivée d'un client.
* Cette méthode gère l'arrivée d'un client à la banque, le fait passer au caissier libre
* s'il y en a un, sinon le place dans la file d'attente. Ensuite, elle calcule l'échéance
* de la prochaine arrivée de client et crée un nouvel événement d'arrivée s'il se situe
* dans la durée prévue de la simulation.
*/
void Arrivee::traiter() {
    Client* client = new Client(pBanque->heureActuelle());

    Caissier* caissier = pBanque->caissierLibre();
    if (caissier != NULL)
        caissier->servir(client);
    else
        pBanque->fileAttente()->ajouter(client);

    double echeance = pBanque->heureActuelle() + pBanque->tempsMoyenArrivee();


    if (echeance <= pBanque->dureePrevue()) {
        cout << "Evenement Arrivee : Le client est arrive a " << echeance << endl;

        pBanque->incrementerNbClients();
   
        pBanque->ajouter(new Arrivee(echeance, this->pBanque));
    }
}