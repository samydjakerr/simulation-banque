/// @file Client.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Client
/// @date 2023

#include "include/Client.hpp"

/**
 * @brief Constructeur de la classe Client.
 *
 * @param heureArrivee L'heure d'arrivée du client.
 */
Client::Client(double heureArrivee) {
    _heureArrivee = heureArrivee;
}

/**
 * @brief Obtient l'heure d'arrivée du client.
 *
 * @return L'heure d'arrivée.
 */
double Client::heureArrivee() {
    return _heureArrivee;
}
