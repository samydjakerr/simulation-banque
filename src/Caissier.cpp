/// @file Caissier.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Caissier
/// @date 2023

#include "include/Caissier.hpp"
#include "include/Banque.hpp"
#include "include/Depart.hpp"

#include <iostream>
using namespace std;


Caissier::Caissier(double tempsMoyService, Banque* banque) {
    this->_tempsMoyService = tempsMoyService;
    this->_nbClientsServis = 0;
    this->_tauxOccupation = 0;
    this->_libre = true;
    this->pBanque = banque;
}

bool Caissier::estLibre() {
    return _libre;
}

double Caissier::tempsMoyenService() {
    return _tempsMoyService;
}

double Caissier::tauxOccupation() {
    if (pBanque->dureeReelle() != 0)
        return (_tauxOccupation / pBanque->dureeReelle()) * 100;
    else
        return 0;
}

int Caissier::nbClientsServis() {
    return _nbClientsServis;
}

Banque* Caissier::banque() {
    return pBanque;
}

void Caissier::servir(Client* client) {
    this->_nbClientsServis++;
    this->_libre= false;
    //
    double heureDepart = pBanque->heureActuelle() + _tempsMoyService;
   
    this->_tauxOccupation += heureDepart - pBanque->heureActuelle();
    
    cout << "Evenement Depart : Le client est servi et parti a " << heureDepart << " !" << endl;
    pBanque->ajouter(new Depart(heureDepart, this, this->pBanque));
}

void Caissier::attendre() {
    this->_libre = true;

}
