/// @file Banque.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Banque, dérivée de la classe SED 
/// @date 2023

#include "include/Caissier.hpp"
#include "include/FileAttente.hpp"
#include "include/Banque.hpp"
#include "include/Arrivee.hpp"

#include <iostream>
using namespace std;
/**
 * @brief Constructeur de la classe Banque.
 *
 * @param dureePrevue La durée prévue de la simulation.
 * @param tempsMoyArrivee Le temps moyen entre les arrivées des clients.
 * @param nbCaissiers Le nombre de caissiers dans la banque.
 * @param tempsMoyService Le temps moyen de service par caissier.
 */

Banque::Banque(double dureePrevue, double tempsMoyArrivee, int nbCaissiers, double tempsMoyService):SED(_heureActuelle) {
    _dureePrevue = dureePrevue;
    _tempsMoyArrivee = tempsMoyArrivee;
    _nbCaissiers = nbCaissiers;
    _tempsMoyService = tempsMoyService;
    _dureeReelle = 0;
    _nbClients = 0;  
    pCaissiers = new Caissier*[_nbCaissiers];
    pFileAttente = new FileAttente(this);

    for (int i = 0; i < _nbCaissiers; i++) {
        //
        this->pCaissiers[i] = new Caissier(_tempsMoyService, this);
    }
    cout << "Evenement Arrivee : Le client est arrive a 0" << endl;
    this->listEvenement.push(new Arrivee(0, this));
       
}  

/**
 * @brief Obtient la liste des caissiers de la banque.
 *
 * @return Un tableau de pointeurs vers les caissiers.
 */
Caissier** Banque::caissiers() {
    return pCaissiers;
}
/**
 * @brief Obtient la file d'attente de la banque.
 *
 * @return Un pointeur vers la file d'attente.
 */
FileAttente* Banque::fileAttente() {
    return pFileAttente;
}

/**
 * @brief Trouve un caissier libre dans la banque.
 *
 * @return Un pointeur vers un caissier libre ou nullptr s'il n'y en a pas.
 */
Caissier* Banque::caissierLibre() {
    int i = 0;
    Caissier* caissier = nullptr;

    do {
        caissier = pCaissiers[i];
        i++;
    } while (i < _nbCaissiers && !caissier->estLibre());

    if (i <= _nbCaissiers && caissier->estLibre())
        return caissier;
    else
        return nullptr;
}

/**
 * @brief Obtient la durée prévue de la simulation.
 *
 * @return La durée prévue.
 */
double Banque::dureePrevue() {
    return _dureePrevue;
}

/**
 * @brief Obtient la durée réelle de la simulation.
 *
 * @return La durée réelle.
 */
double Banque::dureeReelle() {
    return _dureeReelle;
}



/**
 * @brief Obtient le nombre de clients servis par la banque.
 *
 * @return Le nombre de clients.
 */
int Banque::nbClients() {
    return _nbClients;
}

/**
 * @brief Obtient le temps moyen entre les arrivées des clients.
 *
 * @return Le temps moyen.
 */
double Banque::tempsMoyenArrivee() {
    return _tempsMoyArrivee;
}

/**
 * @brief Incrémente le nombre de clients servis par la banque.
 */
void Banque::incrementerNbClients() {
    _nbClients = _nbClients + 1;
}

/**
 * @brief Lance la simulation en traitant les événements en attente.
 *
 * Cette méthode est responsable de la gestion de la simulation en traitant les événements
 * en attente dans l'ordre de leur occurrence. Elle parcourt la liste des événements et,
 * pour chaque événement, met à jour l'heure actuelle, la longueur moyenne de la file
 * d'attente, puis traite l'événement en appelant sa méthode `traiter()`.
 */

void Banque::lancer() {
    double heureDernierEvenement;
    while (!listEvenement.empty()) {
        Evenement* evenement = listEvenement.top(); 
        evenement = listEvenement.top();
        listEvenement.pop();
        heureDernierEvenement = evenement->heureEvenement();
        _heureActuelle = evenement->heureEvenement();
        fileAttente()->contributionLongueurMoyenne((_heureActuelle - heureDernierEvenement) * fileAttente()->tailleFile());
        evenement->traiter();
        heureDernierEvenement = _heureActuelle; 
    }
}