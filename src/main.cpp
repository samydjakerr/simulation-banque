/// @file main.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Programme de simulation de la gestion d'une banque.
/// @date 2023


#include "include/Banque.hpp"
#include <getopt.h>
#include <typeinfo>
#include <cmath>
#include <queue>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

/// @brief Affiche les paramètres de la simulation.
/// @param argDureePrevue La durée prévue de la simulation.
/// @param argNbCaissiers Le nombre de caissiers.
/// @param argTempsMoyArrivee Le temps moyen d'arrivée des clients.
/// @param argTempsMoyService Le temps moyen de service.

void afficherParametres(double argDureePrevue, int argNbCaissiers, double argTempsMoyArrivee, double argTempsMoyService) {
    std::cout << "--------------------- PARAMETRES DE LA SIMULATION ---------------------------" << std::endl;
    std::cout << std::left << std::setw(39) << "Duree prevue :" << std::left << std::setw(15) << argDureePrevue << std::endl;
    std::cout << std::left << std::setw(37) << "Nombre de caissiers :" << std::left << std::setw(15) << argNbCaissiers << std::endl;
    std::cout << std::left << std::setw(38) << "Temps moyen d'arrivee des clients :" << std::left << std::setw(15) <<argTempsMoyArrivee << std::endl;
    std::cout << std::left << std::setw(37) << "Temps de service moyen :" << std::left << std::setw(15) << argTempsMoyService << std::endl;
}
/// @brief Affiche les statistiques globales de la simulation.
/// @param dureeReelle La durée réelle de la simulation.
/// @param nbClients Le nombre total de clients servis.
/// @param _longeurMax La longueur maximale de la file d'attente.
/// @param _longeurMoy La longueur moyenne de la file d'attente.
void afficherStatistiquesGlobales(double dureeReelle, int nbClients, double _longeurMax, double _longeurMoy) {
        std::cout << "------------------------- STATISTIQUES GLOBALES ----------------------------" << std::endl;
        std::cout << std::left << std::setw(39) << "Duree reelle :" << std::left << std::setw(15) << dureeReelle << std::endl;
        std::cout << std::left << std::setw(37) << "Nombre de clients servis au total :" << std::left << std::setw(15) << nbClients + 1 << std::endl;
        std::cout << std::left << std::setw(37) << "Longueur maximum de la file :" << std::left << std::setw(15) << _longeurMax << std::endl;
        std::cout << std::left << std::setw(37) << "Longueur moyenne de la file :" << std::left << std::setw(15) << _longeurMoy << std::endl;
}
/// @brief Affiche les statistiques des caissiers.
/// @param argNbCaissiers Le nombre de caissiers.
/// @param _tauxOccupation Tableau des taux d'occupation des caissiers.
/// @param _nbClientsServis Tableau du nombre de clients servis par les caissiers.
/// @param _tempsMoyService Tableau des temps moyens de service des caissiers.
/// @param pBanque Un pointeur vers l'objet Banque.
void afficherStatistiquesCaissiers(int argNbCaissiers, double* _tauxOccupation, int* _nbClientsServis, double*_tempsMoyService, Banque* pBanque) 
{
    std::cout << "--------------------------- STATISTIQUES CAISSIERS -------------------------" << std::endl;
    std::stringstream string_stream;
        for (int i = 0; i < argNbCaissiers; i++) {
            string_stream.str(""); string_stream.clear();
            string_stream << pBanque->caissiers()[i]->tauxOccupation();
            
            std::cout << "Caissier " << i + 1 << std::endl;
            std::cout << std::left << std::setw(37) << "\t- Temps moyen de service :" << std::left << std::setw(15) << pBanque->caissiers()[i]->tempsMoyenService() << std::endl;
            std::cout << std::left << std::setw(37) << "\t- Taux d'occupation:" << std::left << std::setw(15) << string_stream.str() + " %" << std::endl;
            std::cout << std::left << std::setw(37) << "\t- Nombre de clients servis:" << std::left << std::setw(15) << pBanque->caissiers()[i]->nbClientsServis() << std::endl;
        }
    std::cout << "" << std::endl;
    std::cout << "=============================== Fin de Programme ===========================" << std::endl;
}



/// @brief Fonction principale du programme.
int main(int argc, char** argv) {
    double argDureePrevue = 0;
    double argTempsMoyArrivee = 0;
    int argNbCaissiers = 0;
    double argTempsMoyService = 0;

    do {
        // Demande à l'utilisateur de saisir les valeurs
        std::cout << "Entrez la duree prevue : ";
        std::cin >> argDureePrevue;

        std::cout << "Entrez le temps moyen d'arrivee des clients  : ";
        std::cin >> argTempsMoyArrivee;

        std::cout << "Entrez le temps moyen de service  : ";
        std::cin >> argTempsMoyService;

        std::cout << "Entrez le nombre de caissiers  : ";
        std::cin >> argNbCaissiers;

        if (argDureePrevue <= 0 || argTempsMoyArrivee <= 0 || argTempsMoyService <= 0 || argNbCaissiers <= 0) {
            std::cerr << "Les valeurs doivent être strictement positives." << std::endl;
        }
    } while (argDureePrevue <= 0 || argTempsMoyArrivee <= 0 || argTempsMoyService <= 0 || argNbCaissiers <= 0);

    
    cout << "======================== DEBUT DE LA SIMULATION A " << "0" << " ====================" << endl;

    Banque *pBanque = new Banque(argDureePrevue, argTempsMoyArrivee, argNbCaissiers, argTempsMoyService);
    pBanque->lancer();

    cout << "=========================FIN DE LA SIMULATION ==============================" << endl;
    std::cout << "" << std::endl;
    std::cout << "" << std::endl;
    double _heureActuelle= pBanque->heureActuelle();
    int _nbClients = pBanque->nbClients();
    double _longeurMax=pBanque->fileAttente()->longueurMax();
    double _longeurMoy =pBanque->fileAttente()->longueurMoy();
    double* _tauxOccupation=0;
    int* _nbClientsServis=0;
    double* _tempsMoyService=0;
    afficherParametres(argDureePrevue, argNbCaissiers, argTempsMoyArrivee, argTempsMoyService);
    afficherStatistiquesGlobales(_heureActuelle, _nbClients, _longeurMax, _longeurMoy);
    afficherStatistiquesCaissiers(argNbCaissiers, _tauxOccupation,_nbClientsServis, _tempsMoyService, pBanque);
    
    return 0;
}
