/// @file Depart.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Depart, dérivée de la classe Evenement
/// @date 2023

#include "include/Depart.hpp"
#include "include/Banque.hpp"

#include <iostream>
using namespace std;
class FileAttente;

/**
 * @brief Constructeur de la classe Depart.
 *
 * @param _heureEvenement L'heure à laquelle l'événement de départ se produit.
 * @param pCaissier Un pointeur vers l'objet Caissier associé à l'événement.
 * @param pBanque Un pointeur vers l'objet Banque associé à l'événement.
 */
Depart::Depart(double _heureEvenement, Caissier* pCaissier, Banque* pBanque):Evenement(_heureEvenement, pBanque) {
    this->_heureEvenement = _heureEvenement;
    this->pCaissier = pCaissier;
    this->pBanque = pBanque;
}

/**
 * @brief Traite l'événement de départ d'un client.
 *
 * Cette méthode gère le départ d'un client du caissier. Si la file d'attente n'est pas vide,
 * elle retire un client de la file et le fait servir par le caissier. Sinon, le caissier est mis
 * en attente pour servir le prochain client.
 */
void Depart::traiter() {
    FileAttente* fileClients = pBanque->fileAttente();
    if (!fileClients->estVide()) {
        Client* client = fileClients->retirer();
        pCaissier->servir(client);
    } else {
        pCaissier->attendre();
    }
}
