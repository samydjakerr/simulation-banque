/// @file Evenement.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe Evenement
/// @date 2023

#include "include/Evenement.hpp"

/**
 * @brief Constructeur de la classe Evenement.
 *
 * @param _heureEvenement L'heure à laquelle l'événement se produit.
 * @param pBanque Un pointeur vers l'objet Banque associé à l'événement.
 */
class Evenement;
Evenement::Evenement(double _heureEvenement, Banque* pBanque) {
    this->_heureEvenement = _heureEvenement;
    this->pBanque = pBanque;

}

/**
 * @brief Constructeur par défaut de la classe Evenement.
 */
Evenement::Evenement() {}

/**
 * @brief Obtient l'heure de l'événement.
 *
 * @return L'heure de l'événement.
 */
double Evenement::heureEvenement() {
    return _heureEvenement;
}

/**
 * @brief Traite l'événement.
 *
 * Cette méthode est redéfinie dans les classes dérivées Arrivee et Depart pour implémenter le traitement
 * spécifique de chaque type d'événement.
 */
void Evenement::traiter() {}
