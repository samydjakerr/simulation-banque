/// @file Arrivee.hpp

#ifndef ARRIVEE_HPP_
#define ARRIVEE_HPP_
#include "Evenement.hpp"

/// Classe représentant un événement d'arrivée de client dans une banque
class Arrivee : public Evenement {
public:
    /// Constructeur de la classe Arrivee
    /// @param heureEvenement L'heure à laquelle l'événement d'arrivée se produit
    /// @param pBanque Pointeur vers l'objet Banque associé
    Arrivee(double heureEvenement, Banque* pBanque);

    /// Méthode pour traiter l'événement d'arrivée
    virtual void traiter() override;
};

#endif
