/// @file Client.hpp
#ifndef CLIENT_HPP_
#define CLIENT_HPP_

/// Classe représentant un client dans la simulation
class Client {
protected:
    double _heureArrivee; ///< Heure à laquelle le client est arrivé

public:
    /// Constructeur de la classe Client
    /// @param heureArrivee Heure à laquelle le client est arrivé
    Client(double heureArrivee);

    /// Méthode pour obtenir l'heure d'arrivée du client
    /// @return L'heure d'arrivée du client
    double heureArrivee();
};

#endif
