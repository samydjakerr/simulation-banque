/// @file SED.hpp
#ifndef SED_HPP_
#define SED_HPP_

#include <queue>
#include <vector>
#include "Evenement.hpp"

using namespace std;

/// Foncteur de comparaison personnalisé pour la file de priorité
class CompareEvenement {
public:
    int operator()(Evenement* evenement1, Evenement* evenement2) {
        return evenement1->heureEvenement() > evenement2->heureEvenement();
    }
};

/// Classe SED (Simulation à Événements Discrets)
class SED {
protected:
    double _heureActuelle; ///< Heure actuelle de la simulation
    priority_queue<Evenement*, vector<Evenement*>, CompareEvenement> listEvenement; ///< File de priorité des événements

public:
    /// Constructeur de la classe SED
    /// @param heureActuelle L'heure initiale de la simulation
    SED(double heureActuelle);

    /// Accesseur pour obtenir la liste des événements planifiés
    /// @return La file de priorité des événements
    const priority_queue<Evenement*, vector<Evenement*>, CompareEvenement>& evenementList() const;

    /// Accesseur pour obtenir l'heure actuelle de la simulation
    /// @return L'heure actuelle de la simulation
    virtual double heureActuelle();

    /// Méthode pour ajouter un événement à la liste des événements planifiés
    /// @param evenement L'événement à ajouter
    void ajouter(Evenement* evenement);

    /// Méthode pour lancer la simulation
    virtual void lancer();
};

#endif
