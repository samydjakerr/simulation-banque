/// @file Caissier.hpp

#ifndef CAISSIER_HPP_
#define CAISSIER_HPP_
#include "Depart.hpp"
#include "Client.hpp"
/// Inclusion des dépendances

class Banque;


/// Classe représentant un caissier dans la simulation de la banque
class Caissier {
protected:
    bool _libre;                   ///< Indique si le caissier est libre
    double _tempsMoyService;       ///< Temps moyen de service du caissier
    int _nbClientsServis;          ///< Nombre de clients servis par le caissier
    double _tauxOccupation;        ///< Taux d'occupation du caissier
    Banque* pBanque;               ///< Pointeur vers l'instance de la banque

public:
    /// Constructeur de la classe Caissier
    /// @param tempsMoyService Temps moyen de service du caissier
    /// @param pBanque Pointeur vers l'instance de la banque
    Caissier(double tempsMoyService, Banque* pBanque);

    /// Méthode pour obtenir le temps moyen de service du caissier
    /// @return Le temps moyen de service du caissier
    double tempsMoyenService();

    /// Méthode pour obtenir le nombre de clients servis par le caissier
    /// @return Le nombre de clients servis
    int nbClientsServis();

    /// Méthode pour obtenir le taux d'occupation du caissier
    /// @return Le taux d'occupation en pourcentage
    double tauxOccupation();

    /// Méthode pour vérifier si le caissier est libre
    /// @return Vrai si le caissier est libre, sinon faux
    bool estLibre();

    /// Méthode pour obtenir un pointeur vers l'instance de la banque
    /// @return Pointeur vers la banque
    Banque* banque();

    /// Méthode pour faire servir un client par le caissier
    /// @param client Pointeur vers le client à servir
    void servir(Client* client);

    /// Méthode pour mettre le caissier en attente
    void attendre();

};

#endif
