/// @file Depart.hpp
#ifndef DEPART_HPP_
#define DEPART_HPP_

#include "Evenement.hpp"

/// Déclaration préalable des classes Caissier et Client
class Caissier;
class Client;

/// Classe représentant un événement de départ dans la simulation
class Depart : public Evenement {
protected:
    Caissier* pCaissier; ///< Pointeur vers le caissier associé au départ
    Client* pClient; ///< Pointeur vers le client associé au départ

public:
    /// Constructeur de la classe Depart
    /// @param _heureEvenement Heure à laquelle l'événement de départ doit se produire
    /// @param pCaissier Pointeur vers le caissier associé au départ
    /// @param pBanque Pointeur vers l'objet Banque associé à la simulation
    Depart(double _heureEvenement, Caissier* pCaissier, Banque* pBanque);

    /// Méthode pour traiter l'événement de départ
    void traiter();
};

#endif
