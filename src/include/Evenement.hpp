/// @file Evenement.hpp
#ifndef EVENEMENT_HPP_
#define EVENEMENT_HPP_


class Banque; // Ajout de la déclaration de la classe Banque

/// Classe de base abstraite pour représenter un événement dans la simulation
class Evenement {
protected:
    Banque* pBanque; ///< Pointeur vers l'objet Banque associé
    double _heureEvenement; ///< Heure à laquelle l'événement doit se produire


public:
    /// Constructeur de la classe Evenement
    /// @param _heureEvenement Heure à laquelle l'événement doit se produire
    /// @param pBanque Pointeur vers l'objet Banque associé
    Evenement(double _heureEvenement, Banque* pBanque);

    /// Constructeur par défaut de la classe Evenement
    Evenement();

    /// Accesseur pour obtenir l'heure de l'événement
    /// @return L'heure de l'événement
    virtual double heureEvenement();

    /// Méthode virtuelle pour traiter l'événement
    virtual void traiter() = 0;
};

#endif
