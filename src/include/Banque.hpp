/// @file Banque.hpp

#ifndef BANQUE_HPP_
#define BANQUE_HPP_

#include "Caissier.hpp"
#include "FileAttente.hpp"
#include "Client.hpp"
#include "SED.hpp"
#include "Banque.hpp"

/// Classe représentant la simulation d'une banque
class Banque:public SED {
protected:
    int _nbCaissiers;             ///< Nombre de caissiers dans la banque
    int _nbClients;               ///< Nombre de clients dans la banque
    double _tempsMoyService;      ///< Temps moyen de service dans la banque
    double _dureePrevue;          ///< Durée prévue de la simulation
    double _dureeReelle;          ///< Durée réelle de la simulation
    double _tempsMoyArrivee;     ///< Temps moyen d'arrivée des clients
    Caissier** pCaissiers;        ///< Tableau de pointeurs vers les caissiers de la banque
    FileAttente* pFileAttente;    ///< File d'attente des clients

public:
    /// Constructeur de la classe Banque
    /// @param dureePrevue Durée prévue de la simulation
    /// @param tempsMoyArrivee Temps moyen d'arrivée des clients
    /// @param nbCaissiers Nombre de caissiers dans la banque
    /// @param tempsMoyService Temps moyen de service dans la banque
    Banque(double dureePrevue, double tempsMoyArrivee, int nbCaissiers, double tempsMoyService);

    /// Méthode pour obtenir un tableau de pointeurs vers les caissiers
    /// @return Tableau de pointeurs vers les caissiers de la banque
    Caissier** caissiers();

    /// Méthode pour obtenir la file d'attente des clients
    /// @return Pointeur vers la file d'attente
    FileAttente* fileAttente();

    /// Méthode pour obtenir un caissier libre (non occupé)
    /// @return Pointeur vers un caissier libre, sinon nullptr
    Caissier* caissierLibre();

    /// Méthode pour obtenir la durée prévue de la simulation
    /// @return La durée prévue
    double dureePrevue();

    /// Méthode pour obtenir la durée réelle de la simulation
    /// @return La durée réelle
    double dureeReelle();

    /// Méthode pour obtenir le nombre de clients dans la banque
    /// @return Le nombre de clients
    int nbClients();

    /// Méthode pour obtenir le temps moyen d'arrivée des clients
    /// @return Le temps moyen d'arrivée
    double tempsMoyenArrivee();



    /// Méthode pour incrémenter le nombre de clients dans la banque
    void incrementerNbClients();

    /// Méthode pour lancer la simulation de la banque
    void lancer();
};

#endif
