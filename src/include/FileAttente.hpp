/// @file FileAttente.hpp
#ifndef FILEATTENTE_HPP_
#define FILEATTENTE_HPP_

#include <deque>

using namespace std;

class Banque;
class Client; // Ajout de la déclaration de la classe Client

/// Classe FileAttente pour représenter la file d'attente des clients
class FileAttente {
protected:
    int _longueurMax; ///< Longueur maximale de la file d'attente
    double _longueurMoy; ///< Longueur moyenne de la file d'attente
    double _tempsAttenteMoy; ///< Temps d'attente moyen des clients
    Banque* pBanque; ///< Pointeur vers l'objet Banque associé
    deque<Client*> fileClients; ///< File d'attente des clients

public:
    /// Constructeur de la classe FileAttente
    /// @param pBanque Pointeur vers l'objet Banque associé
    FileAttente(Banque* pBanque);

    /// Accesseur pour obtenir la longueur maximale de la file d'attente
    /// @return La longueur maximale de la file d'attente
    int longueurMax();

    /// Accesseur pour obtenir la longueur moyenne de la file d'attente
    /// @return La longueur moyenne de la file d'attente
    double longueurMoy();

    /// Méthode pour ajouter un client à la file d'attente
    /// @param client Le client à ajouter à la file d'attente
    void ajouter(Client* client);

    /// Méthode pour vérifier si la file d'attente est vide
    /// @return Vrai si la file d'attente est vide, sinon faux
    bool estVide();

    /// Méthode pour retirer le client en tête de la file d'attente
    /// @return Le client retiré de la file d'attente
    Client* retirer();

    /// Méthode pour contribuer à la longueur moyenne de la file d'attente
    /// @param contribution La contribution à ajouter à la longueur moyenne
    void contributionLongueurMoyenne(double contribution);

    /// Accesseur pour obtenir la taille actuelle de la file d'attente
    /// @return La taille actuelle de la file d'attente
    int tailleFile();
};

#endif
