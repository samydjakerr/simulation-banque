/// @file SED.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe SED (Simulation par Evenements Discrets)
/// @date 2023

#include "include/SED.hpp"

#include <iostream>

using namespace std;

/// @brief Constructeur de la classe SED.
/// @param heureActuelle L'heure actuelle de la simulation.
SED::SED(double heureActuelle) {
    _heureActuelle = heureActuelle;
}

/// @brief Obtient l'heure actuelle de la simulation.
/// @return L'heure actuelle.
double SED::heureActuelle() {
    return _heureActuelle;
}

/// @brief Obtient la liste des événements en attente dans la simulation.
/// @return Une référence constante à la liste des événements.
const priority_queue<Evenement*, vector<Evenement*>, CompareEvenement>& SED::evenementList() const {
    return listEvenement;
}

/// @brief Ajoute un événement à la liste d'événements en attente.
/// @param evenement L'événement à ajouter.
void SED::ajouter(Evenement* evenement) {
    this->listEvenement.push(evenement);
}

/// @brief Lance la simulation en traitant les événements en attente.
/// Cette méthode est implementée dans la classe dérivée Banque pour exécuter la simulation en traitant les événements
/// dans l'ordre de leur occurrence.
 
void SED::lancer() {
  
}
