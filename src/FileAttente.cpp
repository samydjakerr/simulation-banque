/// @file FileAttente.cpp
/// @author Ryma HADJI & Samy Djakker
/// @brief Implémente la classe FileAttente
/// @date 2023

#include "include/FileAttente.hpp"
#include "include/Banque.hpp"
#include <iostream>
using namespace std;

/**
 * @brief Constructeur de la classe FileAttente.
 *
 * @param banque Un pointeur vers l'objet Banque associé à la file d'attente.
 */
FileAttente::FileAttente(Banque* banque) {
    this->_longueurMax = 0;
    this->_longueurMoy = 0;
    this->_tempsAttenteMoy = 0;
    this->pBanque = banque;
}

/**
 * @brief Obtient la longueur maximale de la file d'attente.
 *
 * @return La longueur maximale.
 */
int FileAttente::longueurMax() {
    return _longueurMax;
}

/**
 * @brief Obtient la longueur moyenne de la file d'attente.
 *
 * @return La longueur moyenne.
 */
double FileAttente::longueurMoy() {
    if (pBanque->dureeReelle() != 0)
        return _longueurMoy / pBanque->dureeReelle();
    else
        return 0;
}

/**
 * @brief Obtient la taille actuelle de la file d'attente.
 *
 * @return La taille de la file d'attente.
 */
int FileAttente::tailleFile() {
    return this->fileClients.size();
}

/**
 * @brief Ajoute un client à la file d'attente.
 *
 * @param client Le client à ajouter.
 */
void FileAttente::ajouter(Client* client) {
    cout << "Aucun caissier libre ! Le client rejoint la file à la position " << this->fileClients.size() + 1 << endl;
    this->fileClients.push_back(client);
    if (fileClients.size() > _longueurMax)
        this->_longueurMax = fileClients.size();
}

/**
 * @brief Contribue à la longueur moyenne de la file d'attente.
 *
 * @param contribution La contribution à ajouter à la longueur moyenne.
 */
void FileAttente::contributionLongueurMoyenne(double contribution) {
    this->_longueurMoy += contribution;
}

/**
 * @brief Vérifie si la file d'attente est vide.
 *
 * @return true si la file d'attente est vide, sinon false.
 */
bool FileAttente::estVide() {
    return fileClients.empty();
}

/**
 * @brief Retire le premier client de la file d'attente.
 *
 * @return Un pointeur vers le premier client.
 */
Client* FileAttente::retirer() {
    Client* premierClient = this->fileClients.front();
    this->fileClients.pop_front();
    return premierClient;
}
