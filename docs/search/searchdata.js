var indexSectionsWithContent =
{
  0: ":_abcdefhilmnoprstué",
  1: "abcdefs",
  2: "abcdefmrs",
  3: "abcdefhilmnorst",
  4: "_flp",
  5: ":bcdsé"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Pages"
};

